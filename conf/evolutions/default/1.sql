# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table activities (
  id                        bigint not null,
  user_id                   bigint not null,
  created_at                timestamp,
  action_at                 timestamp,
  from_time                 varchar(255),
  to_time                   varchar(255),
  duration_in_min           integer,
  distance_in_km            double,
  kind_id                   bigint,
  comment                   varchar(255),
  kml                       LONGTEXT,
  constraint pk_activities primary key (id))
;

create table items (
  id                        bigint not null,
  user_id                   bigint not null,
  name                      varchar(255),
  picture                   blob,
  created_at                timestamp,
  constraint pk_items primary key (id))
;

create table kinds (
  id                        bigint not null,
  user_id                   bigint not null,
  name                      varchar(255),
  created_at                timestamp,
  icon                      varchar(255),
  constraint pk_kinds primary key (id))
;

create table users (
  id                        bigint not null,
  name                      varchar(255),
  email                     varchar(255),
  home                      varchar(255),
  password                  varchar(255),
  salt                      varchar(255),
  picture                   blob,
  date_of_birth             timestamp,
  registration              timestamp,
  size                      integer,
  weight                    integer,
  constraint pk_users primary key (id))
;


create table activities_items (
  activities_id                  bigint not null,
  items_id                       bigint not null,
  constraint pk_activities_items primary key (activities_id, items_id))
;
create sequence activities_seq;

create sequence items_seq;

create sequence kinds_seq;

create sequence users_seq;

alter table activities add constraint fk_activities_users_1 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_activities_users_1 on activities (user_id);
alter table activities add constraint fk_activities_kind_2 foreign key (kind_id) references kinds (id) on delete restrict on update restrict;
create index ix_activities_kind_2 on activities (kind_id);
alter table items add constraint fk_items_users_3 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_items_users_3 on items (user_id);
alter table kinds add constraint fk_kinds_users_4 foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_kinds_users_4 on kinds (user_id);



alter table activities_items add constraint fk_activities_items_activitie_01 foreign key (activities_id) references activities (id) on delete restrict on update restrict;

alter table activities_items add constraint fk_activities_items_items_02 foreign key (items_id) references items (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists activities;

drop table if exists activities_items;

drop table if exists items;

drop table if exists kinds;

drop table if exists users;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists activities_seq;

drop sequence if exists items_seq;

drop sequence if exists kinds_seq;

drop sequence if exists users_seq;

