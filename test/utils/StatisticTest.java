package utils;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Activity;
import models.Kind;
import models.User;

import org.junit.Assert;
import org.junit.Test;

public class StatisticTest {

	private final DateFormat formatter = new SimpleDateFormat( "dd-MM-yyyy" );
	
	private List<Activity> getActivities() {
		
		Activity a1 = new Activity();
		a1.distanceInKm = 10;
		
		Activity a2 = new Activity();
		a2.distanceInKm = 10;
		
		Activity a3 = new Activity();
		a3.distanceInKm = 20;
		
		List<Activity> activities = new ArrayList<Activity>();
		activities.add(a1);
		activities.add(a2);
		activities.add(a3);
		
		return activities;
	}
	
	@Test
	public void filter() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				try {
					
				new Mocker().mock();

				User tad = User.find.byId((long) 2);
				Kind biking = Kind.find.byId((long) 4);
				Kind running = Kind.find.byId((long) 3);

				Date from = formatter.parse("01-01-2013");
				Date to = formatter.parse("01-01-2014");
				
				// get all of this kind!
				Assert.assertEquals(17, Statistic.filter(tad, null, null, biking, from, to).size());	
				Assert.assertEquals(21, Statistic.filter(tad, null, null, running, from, to).size());	
				
				// try to get different kinds
				Assert.assertEquals(0, Statistic.filter(tad, null, biking.id + "", running, from, to).size());
				Assert.assertEquals(0, Statistic.filter(tad, null, running.id + "", biking, from, to).size());
				
				// try to get same kinds
				Assert.assertEquals(17, Statistic.filter(tad, null, biking.id + "", biking, from, to).size());
				Assert.assertEquals(21, Statistic.filter(tad, null, running.id + "", running, from, to).size());
				
				} 
				catch (ParseException e) {

					Assert.fail();
				}
			}
		});
	}
	
	@Test
	public void sumOfDistance() {
		
		List<Activity> activities = getActivities();
		Assert.assertEquals(new Integer(40), Statistic.sumOfDistance(activities));
	}

	@Test
	public void averageOfDistance() {

		List<Activity> activities = getActivities();
		Assert.assertEquals(new Integer(13), Statistic.averageOfDistance(activities));
	}

	@Test
	public void numberOf() {

		List<Activity> activities = getActivities();
		Assert.assertEquals(new Integer(3), Statistic.numberOf(activities));
	}
}