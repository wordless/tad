package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DateHelperTest {

	private final DateFormat formatter = new SimpleDateFormat( "dd-MM-yyyy" );
	
	@Test
	public void calculateDuration() {
		
		Assert.assertEquals(0, DateHelper.calculateDuration("", ""));
		Assert.assertEquals(0, DateHelper.calculateDuration(null, null));
		Assert.assertEquals(0, DateHelper.calculateDuration("14:00", "14:00"));
		Assert.assertEquals(60, DateHelper.calculateDuration("14:00", "15:00"));
	}
	
	@Test
	public void getMonth() throws ParseException {

		Date test = formatter.parse("15-03-2013");
		Date expected = formatter.parse("01-03-2013");
		Date notExpected = formatter.parse("08-03-2013");
		
		Assert.assertEquals(expected, DateHelper.getMonth(test));
		Assert.assertNotEquals(notExpected, DateHelper.getMonth(test));
	}
	
	@Test
	public void getNextDay() throws ParseException {

		Date test = formatter.parse("15-03-2013");
		Date expected = formatter.parse("17-03-2013");		// + 1 Day (skip)
		Date notExpected = formatter.parse("08-03-2013");
		
		Assert.assertEquals(expected, DateHelper.getNextDay(test));
		Assert.assertNotEquals(notExpected, DateHelper.getNextDay(test));
	}
	
	@Test
	public void getNextWeek() throws ParseException {

		Date test = formatter.parse("15-03-2013");
		Date expected = formatter.parse("22-03-2013");		// + 1 Week
		Date notExpected = formatter.parse("08-03-2013");
		
		Assert.assertEquals(expected, DateHelper.getNextWeek(test));
		Assert.assertNotEquals(notExpected, DateHelper.getNextWeek(test));
	}
	
	@Test
	public void getNextMonth() throws ParseException {

		Date test = formatter.parse("15-03-2013");
		Date expected = formatter.parse("01-04-2013");		// + 1 Month
		Date notExpected = formatter.parse("08-03-2013");
		
		Assert.assertEquals(expected, DateHelper.getNextMonth(test));
		Assert.assertNotEquals(notExpected, DateHelper.getNextMonth(test));
	}
	
	@Test
	public void getNextYear() throws ParseException {

		Date test = formatter.parse("15-03-2013");
		Date expected = formatter.parse("01-03-2014");		// + 1 Year
		Date notExpected = formatter.parse("08-03-2013");
		
		Assert.assertEquals(expected, DateHelper.getNextYear(test));
		Assert.assertNotEquals(notExpected, DateHelper.getNextYear(test));
	}
}