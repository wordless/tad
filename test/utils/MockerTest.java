package utils;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import models.DBTester;

import org.junit.Test;

public class MockerTest {

	@Test
	public void mock() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				DBTester.assertEmptyDB();
				
				new Mocker().mock();
				
				DBTester.assertPopulatedDB();
				
				new Mocker().unmock();
				
				DBTester.assertEmptyDB();
			}
		});
    }
}
