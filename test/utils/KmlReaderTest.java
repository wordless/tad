package utils;

import org.junit.Assert;

import org.junit.Test;

public class KmlReaderTest {

	@Test
	public void getDistanceTest() {
		
		String kml8 = new KmlReader().read("mocks/test_8_km.kml");
		String kml45 = new KmlReader().read("mocks/test_45_km.kml");
		String kml56 = new KmlReader().read("mocks/test_56_km.kml");
	    
		double distance8 = new KmlReader().getDistance(kml8);
		double distance45 = new KmlReader().getDistance(kml45);
		double distance56 = new KmlReader().getDistance(kml56);
		
		Assert.assertEquals(8, distance8, 1);
		Assert.assertEquals(45, distance45, 1);
		Assert.assertEquals(56, distance56, 1);
	}
}