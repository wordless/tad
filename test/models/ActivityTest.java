package models;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import utils.DateHelper;
import utils.Mocker;

public class ActivityTest {

	public void mock() {
		
		new Mocker().mock();
	}
	
	@Test
	public void betweenDateAll() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();

				Date fromDate = DateHelper.toDate( "01-01-1970" );
				Date toDate = DateHelper.toDate( "01-01-2070" );
				
				List<Activity> activities = Activity.find.where().between("action_at", fromDate, toDate).findList();

				assertThat(activities.size()).isEqualTo(38);
			}
		});
    }
	
	@Test
	public void betweenDateSome() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();

				Date fromDate = DateHelper.toDate( "01-05-2013" );
				Date toDate = DateHelper.toDate( "01-06-2013" );
				
				List<Activity> activities = Activity.find.where().between("action_at", fromDate, toDate).findList();

				assertThat(activities.size()).isEqualTo(14);
			}
		});
    }
	
	@Test
	public void gtDateSome() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();

				Date fromDate = DateHelper.toDate( "01-05-2013" );
				
				List<Activity> activities = Activity.find.where().gt("action_at", fromDate).findList();

				assertThat(activities.size()).isEqualTo(14);
			}
		});
    }
	
	@Test
	public void ltDateSome() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();

				Date fromDate = DateHelper.toDate( "01-06-2013" );
				
				List<Activity> activities = Activity.find.where().lt("action_at", fromDate).findList();

				assertThat(activities.size()).isEqualTo(36);
			}
		});
    }
}
