package models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Assert;

import play.db.DB;

public class DBTester {

	public static void assertEmptyDB() {

		assertTable("users", 0);
		assertTable("activities", 0);
		assertTable("items", 0);
		assertTable("kinds", 0);
		assertTable("activities_items", 0);
	}

	public static void assertPopulatedDB() {

		assertTable("users", 2);
		assertTable("activities", 38);
		assertTable("items", 3);
		assertTable("kinds", 4);
		assertTable("activities_items", 51);
	}
	
	public static void assertTable(String table, int count) {
		
		try {
			
			Connection connection = DB.getConnection();
		
			Statement statement = connection.createStatement();
			ResultSet result = null;
			
			statement.execute("SELECT count(*) AS c FROM " + table);
			result = statement.getResultSet();
			result.next();
			Assert.assertEquals(table + " contains " + result.getInt("c") + " entries but should contain " + count, result.getInt("c"), count);
			
			connection.close();
		} 
		catch (SQLException e) {

			throw new RuntimeException(e);
		}
	}
}