package models;
import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import models.User;

import org.junit.Test;

import utils.Mocker;
import authentication.HashUtil;

import com.avaje.ebean.ExpressionList;

public class UserTest {

	public void mock() {
		
		new Mocker().mock();
	}
	
	public void unmock() {
		
		new Mocker().unmock();
	}
	
	@Test
	public void authenticate() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
			
				mock();
	
				User tad = User.find.byId(new Long(2));
				
				User user = HashUtil.authenticate("tad@tad.de", "123456", tad.salt);

				assertThat(user.name).isEqualTo("tad");
				assertThat(user.size).isEqualTo(180);
				assertThat(user.home).isEqualTo("Stuttgart");
			}
		});
    }
	
	@Test
	public void findUserById() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();
				
				User user = User.find.byId((long) 2);

				assertThat(user.name).isEqualTo("tad");
				assertThat(user.size).isEqualTo(180);
				assertThat(user.home).isEqualTo("Stuttgart");
			}
		});
    }
	
	@Test
	public void findUserByName() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {

				mock();
				
				ExpressionList<User> users = User.find.where().like("name", "tad");

				assertThat(users.findList().size()).isEqualTo(1);
			}
		});
    }
}