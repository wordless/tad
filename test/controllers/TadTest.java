package controllers;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import models.User;

import org.codehaus.jackson.JsonNode;
import org.junit.Test;

import play.libs.Json;
import play.mvc.Http.Status;
import play.mvc.Result;
import play.test.FakeRequest;
import play.test.Helpers;
import utils.Mocker;

public class TadTest {

	@Test
	public void getStatistics() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/statistics").withSession("email", tad.email);
				
				@SuppressWarnings("deprecation")
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    String content = Helpers.contentAsString(result);
			    
			    JsonNode json = Json.parse(content);
			    
				assertThat(json.size()).isEqualTo(2);
				assertThat(json.get("labels").size()).isEqualTo(4);
			}
		});
	}
	
	@Test
	public void getActivities() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/activity").withSession("email", tad.email);
				
				@SuppressWarnings("deprecation")
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    String content = Helpers.contentAsString(result);
			    
			    JsonNode json = Json.parse(content);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(38);
			}
		});
	}
	
	@Test
	public void getItems() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/item").withSession("email", tad.email);
				
				@SuppressWarnings("deprecation")
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(3);
			}
		});
	}

	@Test
	public void getKinds() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/kind").withSession("email", tad.email);
				
				@SuppressWarnings("deprecation")
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");

			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(3);
			}
		});
	}

	@Test
	public void deleteActivity() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/delete/activity?id=" + tad.activities.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/activity").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    String content = Helpers.contentAsString(result);
			    
			    JsonNode json = Json.parse(content);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(37);
			}
		});
	}
	
	@Test
	public void deleteItem() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/delete/item?id=" + tad.items.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/item").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(2);
			}
		});
	}

	@Test
	public void deleteKind() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/delete/kind?id=" + tad.kinds.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/kind").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");

			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(2);
			}
		});
	}

	@Test
	public void cloneActivity() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/clone/activity?id=" + tad.activities.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/activity").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    String content = Helpers.contentAsString(result);
			    
			    JsonNode json = Json.parse(content);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(39);
			}
		});
	}
	
	@Test
	public void cloneItem() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/clone/item?id=" + tad.items.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/item").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");
			    
			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(4);
			}
		});
	}

	@Test
	public void cloneKind() {
		
		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			@SuppressWarnings("deprecation")
			public void run() {
			
				new Mocker().mock();
	
				User tad = User.find.byId(new Long(2));
				
				Helpers.routeAndCall(Helpers.fakeRequest("GET", "/tad/clone/kind?id=" + tad.kinds.get(0).id).withSession("email", tad.email));
				
				FakeRequest request = Helpers.fakeRequest("GET", "/tad/return/kind").withSession("email", tad.email);
				
				Result result = Helpers.routeAndCall(request);
				
				assertThat(Helpers.status(result)).isEqualTo(Status.OK);
			    assertThat(Helpers.contentType(result)).isEqualTo("text/plain");
			    assertThat(Helpers.charset(result)).isEqualTo("utf-8");

			    JsonNode json = toJson(result);
			    
				assertThat(json.size()).isEqualTo(3);
				assertThat(json.get("aaData").size()).isEqualTo(4);
			}
		});
	}
	
	protected JsonNode toJson(Result result) {

	    String content = Helpers.contentAsString(result);
	    return Json.parse(content);
	}
}