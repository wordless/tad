package authentication;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import models.User;

import org.junit.Assert;
import org.junit.Test;

import utils.SecureModelFactory;

public class HashUtilTest {

	@Test
	public void isSamePassword() {

		String password = "123456";
		
		String saltedAndHashedPassword = HashUtil.hashAndSalt(password, "abc");
		
		Assert.assertTrue(HashUtil.isSamePassword(saltedAndHashedPassword, "abc", "123456"));
		Assert.assertFalse(HashUtil.isSamePassword(saltedAndHashedPassword, "xyz", "123456"));
		Assert.assertFalse(HashUtil.isSamePassword(saltedAndHashedPassword, "abc", "000000"));
    }
	
	@Test
	public void updatePassword() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {

			public void run() {
				
				User user = SecureModelFactory.createNewUser("tad", "tad@tad.de", "123456");
		
				Assert.assertTrue(HashUtil.isSamePassword(user.password, user.salt, "123456"));
				Assert.assertFalse(HashUtil.isSamePassword(user.password, user.salt, "abcdef"));
				
				HashUtil.updatePassword(user, "abcdef");
				
				Assert.assertTrue(HashUtil.isSamePassword(user.password, user.salt, "abcdef"));
				Assert.assertFalse(HashUtil.isSamePassword(user.password, user.salt, "123456"));
			}
		});
    }
}