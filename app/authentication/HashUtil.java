package authentication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import play.Logger;
import models.User;

public class HashUtil {

	private final static SecureRandom random = new SecureRandom();

	public final static String getRandomString() {
		
		byte bytes[] = new byte[20];
	    random.nextBytes(bytes);
	    return bytes.toString();
	}

	public static User authenticate(String email, String password, String salt) {
		
		Logger.debug("authenticate user email = " + email);

		String saltedPassword = hashAndSalt(password, salt);
		
		return User.find.where().eq("email", email).eq("password", saltedPassword).findUnique();
    }
	
	public static void hashAndSalt(User user) {

		hashAndSalt(user, getRandomString());
	}
	
	public static void hashAndSalt(User user, String salt) {

		user.salt = salt;
		user.password = hashAndSalt(user.password, user.salt);
	}
	
	public static String hashAndSalt(String password, String salt) {
		
		try {

			final MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update((password + salt).getBytes());
			
			return new String(messageDigest.digest());
		}
		catch(NoSuchAlgorithmException e) {
			
			throw new RuntimeException(e);
		}
	}

	public static void updatePassword(User user, String newPassword) {

		if(newPassword != null && !newPassword.equals("")) {
			
			user.password = newPassword;
			hashAndSalt(user);
		}
	}
	
	public static boolean isSamePassword(String saltedPassword, String salt, String clearPassword) {
		
		String hashedPassword = hashAndSalt(clearPassword, salt);
		return saltedPassword.equals(hashedPassword);
	}
}