package authentication;

import models.User;
import play.Logger;

public class Login {
    
    public String email;
    public String password;
    
    public String validate() {
    	
    	Logger.debug("validate user email = " + email);
    	
    	User user = User.find.where().eq("email", email).findUnique();
    	
    	if(user == null){
    		
    		return "User not found";
    	}
    	
        if(HashUtil.authenticate(email, password, user.salt) == null) {
        	
            return "Invalid user or password";
        }
        
        return null;
    }  
}