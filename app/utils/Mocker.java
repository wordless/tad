package utils;

import java.util.Date;

import models.Activity;
import models.Item;
import models.Kind;
import models.User;
import play.Logger;

public class Mocker {

	public void unmock() {
		
		Logger.info("unmock");
		
    	for(User user: User.find.all())
    		SecureModelFactory.deleteUser(user.id);
	}
	
	public String mock() {
		
		Logger.info("mock");
		
		unmock();
    
	    User earl = SecureModelFactory.createNewUser("earl", "earl@earl.de", "123456");
	    earl.home = "Berlin";
	    earl.size = 170;
	    earl.weight = 80;
	    earl.dateOfBirth = new Date();
	    earl.save();
    	
	    User tad = SecureModelFactory.createNewUser("tad", "tad@tad.de", "123456");
	    tad.home = "Stuttgart";
	    tad.size = 180;
	    tad.weight = 70;
	    tad.dateOfBirth = new Date();
	    tad.picture = FileHelper.toByteArray("mocks/tad.jpg");
	    tad.save();

	    String kml8 = new KmlReader().read("mocks/test_8_km.kml");
	    String kml45 = new KmlReader().read("mocks/test_45_km.kml");
	    String kml56 = new KmlReader().read("mocks/test_56_km.kml");
	
	    Kind running = SecureModelFactory.createNewKind();
	    running.name = "Running";
	    running.icon = "running.png";
	    
	    Kind biking = SecureModelFactory.createNewKind();
	    biking.name = "Biking";
	    biking.icon = "cycling.png";

	    tad.kinds.add(running);
	    tad.kinds.add(biking);
	    
	    tad.update();
	    
	    Item bike = SecureModelFactory.createNewItem();
	    bike.name = "My Bike";
	    bike.picture = FileHelper.toByteArray("mocks/bike.jpg");
	    tad.items.add(bike);
	    
	    Item watch = SecureModelFactory.createNewItem();
	    watch.name = "Watch";
	    watch.picture = FileHelper.toByteArray("mocks/watch.jpg");
	    tad.items.add(watch);
	    
	    Item bottle = SecureModelFactory.createNewItem();
	    bottle.name = "bottle";
	    bottle.picture = FileHelper.toByteArray("mocks/bottle.png");
	    tad.items.add(bottle);
	    
	    tad.update();

	    Activity activity;
	     
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "a pretty long bike tour";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml56 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "05-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "just a small run";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "07-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "another small run";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "09-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "a biking tour";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml45 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "12-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "running";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "15-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "running (forest)";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "17-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "biking again";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml45 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "20-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "evening run";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "22-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "biking";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml45 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "24-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "biking (one more time)";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml45 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "26-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "running";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml8 );
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "28-03-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "a long bike tour!";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml56 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "01-04-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "another long bike tour!";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.setKml( kml56 );
    	activity.kind = biking;
    	activity.items.add(watch);
    	activity.items.add(bottle);
    	activity.actionAt = DateHelper.toDate( "04-04-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "running (no kml)";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.distanceInKm = 12;
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.actionAt = DateHelper.toDate( "07-04-2013" );
    	tad.activities.add(activity);
    	
	    activity = SecureModelFactory.createNewActivity(tad);
	    activity.comment = "running (no kml)";
	    activity.createdAt = new Date();
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.distanceInKm = 17;
    	activity.kind = running;
    	activity.items.add(watch);
    	activity.actionAt = DateHelper.toDate( "09-04-2013" );
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "11-04-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "13-04-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "15-04-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);

	    activity = makeActivity(tad, "17-04-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "20-04-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "23-04-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "25-04-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "17-04-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "01-05-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "03-05-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "05-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);

	    activity = makeActivity(tad, "07-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "09-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "11-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "14-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "16-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "18-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "21-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "24-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "26-05-2013", running, kml8);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "29-05-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "01-06-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    activity = makeActivity(tad, "04-06-2013", biking, kml56);
	    activity.items.add(watch);
    	tad.activities.add(activity);
    	
	    tad.update();
	    
	    return new JsonPrinter().print(tad);
	}

	private Activity makeActivity(User user, String date, Kind kind, String kml) {

		if(kind.name.equalsIgnoreCase("Biking")) {

			Activity activity = SecureModelFactory.createNewActivity(user);
		    activity.comment = "running (no kml)";
		    activity.setKml( kml );
		    activity.setFromTime("14:30");
		    activity.setToTime("15:15");
		    activity.distanceInKm = 11;
	    	activity.kind = kind;
	    	activity.actionAt = DateHelper.toDate( date );
	    	
	    	return activity;
		}
		
		if(kind.name.equalsIgnoreCase("Running")) {

			Activity activity = SecureModelFactory.createNewActivity(user);
		    activity.comment = "just out for running";
		    activity.setKml( kml );
		    activity.setFromTime("14:30");
		    activity.setToTime("15:15");
		    activity.distanceInKm = 11;
	    	activity.kind = kind;
	    	activity.actionAt = DateHelper.toDate( date );
	    	
	    	return activity;
		}
		

		Activity activity = SecureModelFactory.createNewActivity(user);
	    activity.comment = "running (no kml)";
	    activity.setFromTime("14:30");
	    activity.setToTime("15:15");
	    activity.distanceInKm = 11;
	    activity.durationInMin = 45;
    	activity.kind = kind;
    	activity.actionAt = DateHelper.toDate( date );
    	
    	return activity;
		
	}
}
