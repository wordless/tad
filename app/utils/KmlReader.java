package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import play.mvc.Http.MultipartFormData.FilePart;

public class KmlReader {

	public static class Point {
		
	    private double lat, lon;

	    public Point(String gps) {
	    	
	        String[] xyz = gps.split(",");
	        lat = Double.parseDouble( xyz[1] );
	        lon = Double.parseDouble( xyz[0] );
	    }
	}
	
	/**
	 * Haversine algorithmus
	 * http://www.movable-type.co.uk/scripts/latlong.html
	 */
	private static class Haversine {
		
	    private static final double R = 6371;

	    public static double compute(Point p1, Point p2) {
	    	
	        double dLat = Math.toRadians(p2.lat-p1.lat);
	        double dLon = Math.toRadians(p2.lon-p1.lon);
	        
	        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	                   Math.cos( Math.toRadians(p1.lat) ) *
	                   Math.cos( Math.toRadians(p2.lat) ) * Math.sin(dLon/2) * Math.sin(dLon/2);
	        
	        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	        double d = R * c;
	        return d;
	    }
	}
	
	public List<Point> getPoints(String kml) {
		
		int  lineStringIndex = kml.indexOf("<LineString>");
		kml = kml.substring(lineStringIndex, kml.length());
		
		int coordinatesIndex = kml.indexOf("<coordinates>");
		kml = kml.substring(coordinatesIndex, kml.length());
		
		int coordinatesEndIndex = kml.indexOf("</coordinates>");
		kml = kml.substring(0, coordinatesEndIndex - 1);
		
		kml = kml.replace("<coordinates>", "");

		List<Point> points = new ArrayList<>();
		
		for(String line: kml.split("\n")) {
			
			points.add( new Point(line.trim()) );
		}
		
		return points;
	}
	
	public double getDistance(String kml) {

		List<Point> points = getPoints(kml);
		
		double distance = 0;
		
		Point last = points.get(0);
		
		for(Point point: points) {
			
			distance += Haversine.compute(point, last);	
			last = point;
		}
		
		// rounding with 3 digits atfer "."
		return (double) Math.round(distance * 1000) / 1000;
	}
	
	public String read(String file) {
		
		StringBuilder builder = new StringBuilder();
		
		Path newFile = Paths.get(file);
		    
		try(BufferedReader reader = Files.newBufferedReader(newFile, Charset.defaultCharset())){
		
			String line = "";
		    
		    while((line = reader.readLine()) != null)
		    	builder.append(" " + line.trim() + "\n");
		      
		}
		catch(IOException e){
			
			throw new RuntimeException(e);
		}
		    
		String kml = builder.toString();
		
		kml = kml.replace("\"", "'");

		return kml.substring(kml.indexOf("<kml"), kml.length());    
	}

	public String read(File file) {

		return read(file.getAbsolutePath());
	}

	public String read(FilePart filePart) {
	
		return read(filePart.getFile());
	}
}