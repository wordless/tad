package utils;

import java.io.File;
import java.io.FileInputStream;

import play.mvc.Http.MultipartFormData.FilePart;

public class FileHelper {

	public static byte[] toByteArray(File file) {
		
		try {
		
			FileInputStream fileInputStream = new FileInputStream(file);
	        byte[] data = new byte[(int) file.length()];
	        fileInputStream.read(data);
	        fileInputStream.close();
	        
	        return data; 
		}
		catch(Exception e) {
			
			throw new RuntimeException(e);
		}
	}

	public static byte[] toByteArray(String path) {

		return toByteArray(new File(path));
	}

	public static byte[] toByteArray(FilePart picture) {

		return toByteArray(picture.getFile());
	}
}