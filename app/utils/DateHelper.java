package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	private static final DateFormat formatter = new SimpleDateFormat( "dd-MM-yyyy" );
	
	private static final DateFormat dayFormatter = new SimpleDateFormat("dd/MM/yyyy");
	private static final DateFormat weekFormatter = new SimpleDateFormat("ww/yyyy");
	private static final DateFormat monthFormatter = new SimpleDateFormat("MM/yyyy");
	private static final DateFormat yearFormatter = new SimpleDateFormat("yyyy");
	
	public static int calculateDuration(String start, String end) {
		
		if(start != null && !start.equals("") && end != null && !end.equals("") ) {

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");  

	        Date d1 = null;
	        Date d2 = null;
	        
	        try {
	        	
	            d1 = format.parse(start);
	            d2 = format.parse(end);
	        } 
	        catch (ParseException e) {
	        	
	           	throw new RuntimeException(e);
	        }    
	
	        long diff = d2.getTime() - d1.getTime();
	        return (int) (diff / (60.0 * 1000.0)); 
		}
		
		return 0;
	}
	
	public static Date toDate(String date) {

		try {
			
			return formatter.parse(date);
		} 
		catch (ParseException e) {

			throw new RuntimeException(e);
		}
	}

	public static String getDayMonthYearLabel(Date date) {

		return dayFormatter.format(date);
	}

	public static String getWeekYearLabel(Date date) {

		return weekFormatter.format(date);
	}
	
	public static String getMonthYearLabel(Date date) {
		
		return monthFormatter.format(date);
	}

	public static String getYearLabel(Date date) {

		return yearFormatter.format(date);
	}
	
	public static Date getMonth(Date date) {
    	
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return new Date(cal.getTimeInMillis());
	}
    
	public static Date getNextDay(Date date) {

		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 24);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		cal.add(Calendar.DAY_OF_MONTH, 1);
		
		return new Date(cal.getTimeInMillis());
	}
    
	public static Date getNextWeek(Date date) {

		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		cal.add(Calendar.DAY_OF_MONTH, 7);
		
		return new Date(cal.getTimeInMillis());
	}
	
	public static Date getNextMonth(Date date) {
    	
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.MONTH, 1);
		
		return new Date(cal.getTimeInMillis());
	}
    
	public static Date getNextYear(Date date) {
    	
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.MONTH, 12);
		
		return new Date(cal.getTimeInMillis());
	}
}