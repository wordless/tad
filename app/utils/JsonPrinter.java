package utils;

import org.codehaus.jackson.map.ObjectMapper;

import play.Logger;
import play.libs.Json;

public class JsonPrinter {
	
	private final ObjectMapper mapper = new ObjectMapper();
	
	// no idea how to solve this without the
	// deprecated method
	@SuppressWarnings("deprecation")
	public String print(Object object) {
		   	
	    try {

			String json = mapper.defaultPrettyPrintingWriter().writeValueAsString(object);
			
			Logger.trace("json = " + json);
			
			return json;
		}
	    catch (Exception e) {
			
	    	return Json.toJson(object).toString();
		} 
	}
	
	public static String flat(Object object) {

	    return Json.toJson(object).toString();
	}
}