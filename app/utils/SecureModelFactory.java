package utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import play.Logger;
import play.data.Form;
import play.mvc.Http.Request;
import authentication.HashUtil;
import models.Activity;
import models.Item;
import models.Kind;
import models.User;

/**
 * This class combines methods to create, delete, modify
 * and validate entities. It should always be used to 
 * ensure a correct handling. It will enforce the following
 * things:
 * 		
 * 		- All entities will be created with correct default values! So
 * 		only valid entities that can instantly be saved will  be returned.
 * 
 * 		- All entities will be deleted completely (e.g. associations
 *		will be removes.
 *
 *		- All entities will have a valid state according to a more
 *		high-level business logic than the pure database model can
 *      provide.
 *      
 *      - All operations will check if an entity belongs to a certain user
 *      if necessary.
 * 
 * Example: A user needs a password. This can be easily ensured in the database
 * model (e.g. require/NOT NULL). However, the password should be hashed and stored
 * together with its salt. This "procedure" can't be defined in the database model.
 * But it is ensured by using the "createNewUser(...)" method.
 * 
 * @author Thomas
 */
public class SecureModelFactory {

	/**
	 * @return a valid but unsaved user
	 */
	public static User createNewUser(String name, String email, String password) {
		
		User user = new User();
		user.name = name;
		user.email = email;
		user.password = password;
		user.registration = new Date();
		user.picture = FileHelper.toByteArray("public/images/no_image.png");
		
		Kind defaultKind = createNewKind();
		defaultKind.name = "Default";
		
		user.kinds.add(defaultKind);

		HashUtil.hashAndSalt(user);
		
		Logger.debug("created new user = " + JsonPrinter.flat(user));
		
		return user;
	}
	
	public static Activity createNewActivity(User user) {
		
		Activity activity = new Activity();
		activity.createdAt = new Date();
		activity.actionAt = new Date();
		activity.comment = "New";
		activity.kind = user.getKind("Default");
		
		Logger.debug("created new activity = " + JsonPrinter.flat(activity));
		
		return activity;
	}
	
	public static Item createNewItem() {
		
		Item item = new Item();
		item.createdAt = new Date();
		item.name = "New";
		item.picture = FileHelper.toByteArray("public/images/no_image.png");
		
		Logger.debug("created new item = " + JsonPrinter.flat(item));
		
		return item;
	}
	
	public static Kind createNewKind() {
		
		Kind kind = new Kind();
		kind.createdAt = new Date();
		kind.icon = "default.png";
		kind.name = "New";
		
		Logger.debug("created new kind = " + JsonPrinter.flat(kind));
		
		return kind;
	}
	
	public static void deleteUser(Long id) {

		User user = User.find.byId(id);
		
		for(Activity activity: user.activities)
			deleteActivity(activity.id, user);
		
		user.delete();
		
		Logger.debug("deleted user = " + id);
	}
	
	public static void deleteActivity(Long id, User user) {

		// ensure the entity belongs to the user!
		Activity activity = user.getActivity(id);
				
		activity.deleteManyToManyAssociations("items");
		activity.delete();
		user.update();
		
		Logger.debug("deleted activity = " + id + " of user = " + user.id);
	}
	
	public static void deleteItem(Long id, User user) {

		// ensure the entity belongs to the user!
		Item item = user.getItem(id);
		
		List<Activity> activities = Activity.find.where()
													.eq("items.id", item.id)
													.findList();

		for(Activity activity: activities) {
		
			activity.items.remove(item);
			activity.update();
		}
		
		item.delete();
		user.update();
		
		Logger.debug("deleted item = " + id + " of user = " + user.id);
	}
	
	public static void deleteKind(Long id, User user) {

		// ensure the entity belongs to the user!
		Kind kind = user.getKind(id);
		
		List<Activity> activities = Activity.find.where()
													.eq("kind_id", kind.id)
													.findList();

		for(Activity activity: activities) {
		
			activity.kind = user.getKind("Default");
			activity.update(activity.id);
		}
		
		kind.delete();
		user.update();
		
		Logger.debug("deleted kind = " + id + " of user = " + user.id);
	}

	public static Activity cloneActivity(User user, Activity activity) {

		Activity clone = createNewActivity(user);
		
		clone.comment = activity.comment;
		clone.actionAt = activity.actionAt;
		clone.distanceInKm = activity.distanceInKm;
		clone.durationInMin = activity.durationInMin;
		clone.items = activity.items;
		clone.kind = activity.kind;
		clone.setToTime(activity.getToTime());
		clone.setFromTime(activity.getFromTime());
		clone.setKml(activity.getKml());
		
		return clone;
	}
	
	public static Item cloneItem(Item item) {

		Item clone = SecureModelFactory.createNewItem();
    	
		clone.picture = item.picture;
		clone.name = "clone of " + item.name;
		
    	return clone;
	}
	
	public static Kind cloneKind(Kind kind) {


    	Kind clone = SecureModelFactory.createNewKind();
    	
    	clone.icon = kind.icon;
    	clone.name = "clone of " + kind.name;
    	
    	return clone;
	}

	public static Activity updateActivity(Activity activity, Form<Activity> form, User user, Request request) {

		Activity updated = form.get();
		updated.items = activity.items;
		updated.setKml(activity.getKml());
		
		String kindName = form.get().kind.name;
		Kind kind = user.getKind(kindName);
		updated.kind = kind;
		
		Map<String,String[]> formData = request.body().asFormUrlEncoded();
		List<Item> items = new ArrayList<Item>();
		String[] itemList = formData.get("item_list");
		if(itemList != null)
		    for (String item : itemList)
		    	if(item != null && !item.equals(""))
		    		items.add( user.getItem(item) );
	    updated.items = items;
	    
	    Logger.info("update activity id = " + updated.id);
	    
	    System.out.println(activity);
	    System.out.println(updated);
	    
	    return updated;
	}

	public static Kind updateKind(Form<Kind> form) {

		Kind updated = form.get();
		
		Logger.info("update kind id = " + updated.id);
		return updated;
	}

	public static Item updateItem(Form<Item> form) {

		Item updated = form.get();
		
		Logger.info("update item id = " + updated.id);
		return updated;
	}
}