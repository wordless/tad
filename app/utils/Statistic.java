package utils;

import java.util.Date;
import java.util.List;

import com.avaje.ebean.ExpressionList;

import models.Activity;
import models.Kind;
import models.User;

public class Statistic {

	public static Integer sumOfDistance(List<Activity> activities) {
		
		int distance = 0;
		for(Activity activity: activities)
			distance += activity.distanceInKm;
		return distance;
	}

	public static Integer averageOfDistance(List<Activity> activities) {

		if(activities.size() != 0)
			return sumOfDistance(activities) / activities.size();
		return 0;
	}
	

	public static Integer minDistance(List<Activity> activities) {
		
		int distance = Integer.MAX_VALUE;
		for(Activity activity: activities) {
			if(activity.distanceInKm < distance)
				distance = (int) activity.distanceInKm;
		}
		
		if(distance == Integer.MAX_VALUE)
			return 0;
		
		return distance;
	}

	public static Integer maxDistance(List<Activity> activities) {

		int distance = Integer.MIN_VALUE;
		for(Activity activity: activities) {
			if(activity.distanceInKm > distance)
				distance = (int) activity.distanceInKm;
		}
		
		if(distance == Integer.MIN_VALUE)
			return 0;
		
		return distance;
	}

	public static Integer numberOf(List<Activity> activities) {

		return activities.size();
	}

	/**
	 * Returns all activities of the given user that
	 * 		- has a item with the given itemId (can be null)
	 * 		- has a kind with the given kindId (can be null)
	 * 		- has a kind of the given kind (can not be null)
	 * 		- is after the given from date (can not be null)
	 * 		- is before the given to date (can not be null)
	 * 
	 * @param user to get the activities from
	 * @param itemId that the activities must have (can be null)
	 * @param kindId that the activities must have (can be null)
	 * @param kind that the activities must have (can not be null)
	 * @param from date (can not be null)
	 * @param to date (can not be null)
	 * @return all activities of the user that match the given criteria
	 */
	public static List<Activity> filter(User user, String itemId, String kindId, Kind kind, Date from, Date to) {

		ExpressionList<Activity> query = user.getActivitiesQuery(); 
		
		if(itemId != null)
			query = query
						.eq("items.id", itemId);
		
		if(kindId != null)
			query = query
						.eq("kind_id", kindId);
		
		return query
					.eq("kind_id", kind.id)
					.gt("action_at", from)
					.lt("action_at", to)
					.findList();
	}

	public static int calculate(String type, List<Activity> activities) {

		if("average".equals(type))
			return averageOfDistance(activities);
		
		else if("number".equals(type))
			return numberOf(activities);
		
		else if("min".equals(type))
			return minDistance(activities);
		
		else if("max".equals(type))
			return maxDistance(activities);
		
		// distance	
		return sumOfDistance(activities);

	}
}