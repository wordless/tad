package utils;

public class ColorTable {

	private String[] colors = {
			
			"rgba(255,99,71, 0.7)",
			"rgba(70,130,180, 0.7)",
			"rgba(154,205,50, 0.7)",
			"rgba(0,128,128, 0.7)",
			"rgba(188,143,143, 0.7)",
			"rgba(0,206,209, 0.7)" 
	};
	
	private int i = 0;
	
	public String next() {

		String color = colors[i];
		
		i = (i + 1)%colors.length;
		
		return color;
	}
}