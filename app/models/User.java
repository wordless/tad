package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.avaje.ebean.ExpressionList;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Max;
import play.data.validation.Constraints.Min;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import utils.JsonPrinter;

@Entity
@Table(name="users")
public class User extends Model {

	public static final long serialVersionUID = 6232436397900993920L;

	@Id
	public Long id;
	
	@Required
	@MinLength(value=2)
	public String name;

	@Required
	@Email
	public String email;
	
	public String home;

	@Required
	@MinLength(value=6)
	public String password;

	public String salt;
	
	@Basic(fetch = FetchType.LAZY)
	@Lob
	@JsonIgnore
	public byte[] picture;

	public Date dateOfBirth;
	
	@Required
	public Date registration = new Date();

	@Min(value = 0)
	@Max(value = 300)
	public int size = 0;

	@Min(value = 0)
	@Max(value = 300)
	public int weight = 0;
	
	@OneToMany(cascade=CascadeType.ALL)
	@OrderBy("id DESC")
	public List<Activity> activities = new ArrayList<>();
	
	@OneToMany(cascade=CascadeType.ALL)
	@OrderBy("id DESC")
	public List<Kind> kinds = new ArrayList<>();

	@OneToMany(cascade=CascadeType.ALL)
	@OrderBy("id DESC")
	public List<Item> items = new ArrayList<>();
	
	public String toString() {
		
		return new JsonPrinter().print(this);
	}
	
	public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

	public Activity getActivity(long identifier) {

    	for(Activity activity: activities) {
  
    		if(activity.id == identifier)
    			return activity;
    	}
    	
		return null;
	} 
	
	public Kind getKind(long identifier) {

    	for(Kind kind: kinds) {
    	
    		if(kind.id == identifier)
    			return kind;
    	}
    	
		return null;
	} 
	
	public Kind getKind(String name) {

    	for(Kind kind: kinds) {
    	
    		if(kind.name.equals(name))
    			return kind;
    	}
    	
		return null;
	} 

	public Item getItem(long identifier) {

    	for(Item item: items) {
    	
    		if(item.id == identifier)
    			return item;
    	}
    	
		return null;
	}

	public Item getItem(String name) {

    	for(Item item: items) {
    	
    		if(item.name.equals(name))
    			return item;
    	}
    	
		return null;
	}

	/**
	 * @return the first (= oldest) activity of this user
	 */
	@Transient
	@JsonIgnore
	public Activity getFirstActivity() {

		return getActivitiesQuery()
								.orderBy("action_at asc")
								.findList()
								.get(0);
	}

	/**
	 * @return the last (= youngest) activity of this user
	 */
	@Transient
	@JsonIgnore
	public Activity getLastActivity() {

		return getActivitiesQuery()
								.orderBy("action_at desc")
								.findList()
								.get(0);
	}

	@Transient
	@JsonIgnore
	public ExpressionList<Activity> getActivitiesQuery() {

		return Activity.find.where()
								.eq("t0.user_id", id);
	}
}