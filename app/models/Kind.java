package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name="kinds")
public class Kind extends Model {

	public static final long serialVersionUID = 6232436397900993920L;
	
	public static Kind defaultKind = new Kind("Default"); 

	@Id
	public Long id;

	@Required
	public String name;
	
	public Date createdAt;
	
	public String icon = "default.png";
	
	public Kind() {
		
		// default
	}
	
	private Kind(String name) {

		this.name = name;
		this.createdAt = new Date();
	}
	
	@Transient
	public int getUsed() {
		
		return Activity.find.where()
								.eq("kind_id", id)
								.findList()
								.size();
	}

	public String toString() {
		
		return name;
	}
	
	public static Finder<Long, Kind> find = new Finder<Long, Kind>(Long.class, Kind.class);

	
}