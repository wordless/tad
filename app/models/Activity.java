package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnore;

import play.data.validation.Constraints.Min;
import play.db.ebean.Model;
import utils.DateHelper;
import utils.JsonPrinter;
import utils.KmlReader;

@Entity
@Table(name="activities")
public class Activity extends Model {

	public static final long serialVersionUID = 6232436397900993920L;
	
	@Id
	public Long id;

	@NotNull
	public Date createdAt = new Date();

	public Date actionAt = new Date();

	private String fromTime;
	
	private String toTime;
	
	@Min(value = 0)
	public int durationInMin;
	
	@Min(value = 0)
	public double distanceInKm;

	public void setFromTime(String start) {
		
		this.fromTime = start;
		this.durationInMin = DateHelper.calculateDuration(this.fromTime, this.toTime);
	}

	public String getFromTime() {
		
		return fromTime;
	}

	public void setToTime(String end) {
		
		this.toTime = end;
		this.durationInMin = DateHelper.calculateDuration(this.fromTime, this.toTime);
	}
	
	public String getToTime() {
		
		return toTime;
	}
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@NotNull
	public Kind kind = Kind.defaultKind;

	@NotNull
	public String comment = "-";

	@JsonIgnore
	@Column(columnDefinition = "LONGTEXT")
	private String kml = "";
	
	@Transient
	public boolean getHasKml() {
		
		if(this.kml == null || this.kml.equals(""))
			return false;
		return true;
	}
	
	public void setKml(String kml) {

		if(kml != null && !kml.equals("")) {
			
			distanceInKm = new KmlReader().getDistance(kml);
		}

		this.kml = kml;
	}
	
	@JsonIgnore
	public String getKml() {
		
		return kml;
	}
	
	@JsonIgnore
	@Transient
	public String getFlatKml() {
		
		if(kml == null)
			return null;
		
		StringBuilder builder = new StringBuilder();
		
		for(String line: kml.split("\n"))
			builder.append(" " + line.trim() + " ");
		
		return builder.toString();
	}

	@ManyToMany
	public List<Item> items = new ArrayList<>();

	public String toString() {
		
		return new JsonPrinter().print(this);
	}
	
	public static Finder<Long, Activity> find = new Finder<Long, Activity>(Long.class, Activity.class); 
}