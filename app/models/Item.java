package models;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import utils.JsonPrinter;

@Entity
@Table(name="items")
public class Item extends Model {

	public static final long serialVersionUID = 6232436397900993920L;

	@Id
	public Long id;

	@Required
	public String name;

	@Basic(fetch = FetchType.LAZY)
	@Lob
	@JsonIgnore
	public byte[] picture;
	
	public Date createdAt;
	
	@Transient
	public int getUsed() {
		
		return Activity.find.where()
								.eq("items.id", id)
								.findList()
								.size();
	}

	@Transient
	public boolean getHasImage() {
		
		if(this.picture == null)
			return false;
		return true;
	}
	
	public String toString() {
		
		return new JsonPrinter().print(this);
	}
	
	public static Finder<Long, Item> find = new Finder<Long, Item>(Long.class, Item.class); 
}