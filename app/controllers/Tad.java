package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Activity;
import models.Item;
import models.Kind;
import models.User;

import org.codehaus.jackson.node.ObjectNode;

import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import utils.ColorTable;
import utils.DateHelper;
import utils.FileHelper;
import utils.JsonPrinter;
import utils.KmlReader;
import utils.SecureModelFactory;
import utils.Statistic;
import views.html.*;
import authentication.HashUtil;
import authentication.Secured;

import com.avaje.ebean.ExpressionList;

@Security.Authenticated(Secured.class)
public class Tad extends Controller {

	public static Result getUser() {

    	return ok(new JsonPrinter().print(user()));
    }

    /**
     * 
     
       labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					data : [65,59,90,81,56,55,40]
				},
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					data : [28,48,40,19,96,27,100]
				}
			]
		} 
		
     * @return
     */
    public static Result getStatistics() {

    	final User user = user();
    	
    	Activity first = user.getFirstActivity();
    	Activity last = user.getLastActivity();

    	Date from = DateHelper.getMonth(first.actionAt);
    	Date to = DateHelper.getNextMonth(first.actionAt);
		
    	String type = getQueryString("type");
		String kindId = getQueryString("kind");
		String itemId = getQueryString("item");
		String periode = getQueryString("periode");
		
    	List<String> periodes = new ArrayList<>();
    	List<Kind> kinds = user.kinds;
    	
    	Map<Long, List<Integer>> mapping = new HashMap<>();
    	for(Kind kind: kinds)
    		mapping.put(kind.id, new ArrayList<Integer>());

    	while(from.before(last.actionAt)) {
    	
    		for(Kind kind: kinds) {

	    		List<Activity> activities = Statistic.filter(user, itemId, kindId, kind, from, to);
	    		
	    		Logger.debug("calculating " + type + " statistic");
	   
	    		int value = Statistic.calculate(type, activities);
	    		
	    		mapping.get(kind.id).add(value);
    		}
			
    		if("days".equalsIgnoreCase(periode)) {
    			
	    		periodes.add(DateHelper.getDayMonthYearLabel(from));
	    		from = DateHelper.getNextDay(from);
	        	to = DateHelper.getNextDay(from);
    		}
    		else if("weeks".equalsIgnoreCase(periode)) {
    			
	    		periodes.add(DateHelper.getWeekYearLabel(from));
	    		from = DateHelper.getNextWeek(from);
	        	to = DateHelper.getNextWeek(from);
    		}
    		else if("years".equalsIgnoreCase(periode)) {
    			
	    		periodes.add(DateHelper.getYearLabel(from));
	    		from = DateHelper.getNextYear(from);
	        	to = DateHelper.getNextYear(from);
    		}
    		else {
    			
	    		periodes.add(DateHelper.getMonthYearLabel(from));
	    		from = DateHelper.getNextMonth(from);
	        	to = DateHelper.getNextMonth(from);
    		}
    	}
    	
    	List<ObjectNode> datasets = new ArrayList<>();
    	
    	ColorTable ct = new ColorTable();
    	
    	for(Kind kind: kinds) {
    	
    		String color = ct.next();
    		
    		ObjectNode dataset = Json.newObject();
    		dataset.put("fillColor", color);
    		dataset.put("strokeColor", "lightgray");
    		dataset.put("data", Json.toJson(mapping.get(kind.id)));
    		dataset.put("kind", kind.name);
    	
    		datasets.add(dataset);
    	}
    	
    	ObjectNode result = Json.newObject();
    	result.put("labels", Json.toJson(periodes));
    	result.put("datasets", Json.toJson(datasets));
    	
    	return ok(new JsonPrinter().print(result));
    }

	public static Result viewDashboard() {

        return ok(dashboard.render(user()));
    }
	
	public static Result viewDeleted() {

        return ok(deleted.render(user()));
    }

	public static Result viewUpdated() {

        return ok(updated.render(user(), getReferer()));
    }
	
	public static Result viewCreated() {

        return ok(created.render(user(), getReferer()));
    }
	
    public static Result viewStatistics() {
	
	    return ok(viewStatistics.render(user()));
	}

	public static Result postKml() {

		MultipartFormData body = request().body().asMultipartFormData();
		FilePart upload = body.getFile("kml");

		if(upload == null) {
			
			return notFound("Upload error");
		}
		
		Form<Activity> form = form(Activity.class).bindFromRequest();
		Activity activity;
		
		User user = user();
		
		if(form.get().id == null) {
			
			activity = SecureModelFactory.createNewActivity(user);
			
			user.activities.add(activity);
			user.save();
		}
		else {
			
	    	activity = user.getActivity(form.get().id);
			
			if(activity == null) {
				
				return notFound("Activity not found");
			}
		}

		activity.setKml( new KmlReader().read(upload) );
		activity.update();

		return returnToReferer();
    }
    
    public static Result deleteKml() {

		Form<Activity> form = form(Activity.class).bindFromRequest();

		User user = user();

		Activity activity = user.getActivity(form.get().id);
		activity.setKml("");
		activity.update();

		return returnToReferer();
    }

	public static Result viewActivity() {
    	
    	Long id = getIdFromRequestQueryString();
    	User user = user();
    	
    	Activity activity = user.getActivity(id);
    	return ok(viewActivity.render(user, activity));
    }
    
    public static Result viewItem() {
		
		Long id = getIdFromRequestQueryString();
		User user = user();
		
		Item item = user.getItem(id);
		List<Activity> activities = Activity.find.where().eq("items.id", item.id).findList();
		return ok(viewItem.render(user, item, activities));
	}

	public static Result viewKind() {
		
		Long id = getIdFromRequestQueryString();
		User user = user();
		
		Kind kind = user.getKind(id);
		List<Activity> activities = Activity.find.where().eq("kind_id", id).findList();
		return ok(viewKind.render(user, kind, activities));
	}

	public static Result cloneActivity() {

		long id = getIdFromRequestQueryString();
		User user = user();
		
		Activity activity = user.getActivity( id );
		Activity clone = SecureModelFactory.cloneActivity(user, activity);
		
		user.activities.add(clone);
		user.save();
		
	    return returnToReferer();
	}

	public static Result cloneKind() {
        
    	long id = getIdFromRequestQueryString();
    	User user = user();
    	
    	Kind kind = user.getKind(id);
    	Kind clone = SecureModelFactory.cloneKind(kind);

    	user.kinds.add(clone);
    	user.save();
    	
        return returnToReferer();
    }
    
    public static Result cloneItem() {
	
		long id = getIdFromRequestQueryString();
		User user = user();
		
		Item item = user().getItem( id );
		Item clone = SecureModelFactory.cloneItem(item);

		user.items.add(clone);
		user.save();
		
	    return returnToReferer();
	}

	public static Result viewEditProfil() {
	
		// immutable, returns a new form!
		Form<User> userForm = form(User.class).bindFromRequest().fill(user());

	    return ok(editProfil.render(user(), userForm));
	}

	public static Result viewEditActivity() {
	
		Long id = getIdFromRequestQueryString();
		
		Activity activity;
		
		if(id != null)
			activity = Activity.find.byId(id);
		else
			activity = SecureModelFactory.createNewActivity(user());

		// immutable, returns a new form!
		Form<Activity> form = form(Activity.class).bindFromRequest().fill(activity);
		
	    return ok(editActivity.render(user(), form));
	}

	public static Result viewEditKind() {
	
		Long id = getIdFromRequestQueryString();
		
		Kind kind;
		
		if(id != null)
			kind = Kind.find.byId(id);
		else
			kind = SecureModelFactory.createNewKind();

		// immutable, returns a new form!
		Form<Kind> form = form(Kind.class).bindFromRequest().fill(kind);
		
	    return ok(editKind.render(user(), form));
	}

	public static Result viewEditItem() {
	
		Long id = getIdFromRequestQueryString();
		
		Item item;
		
		if(id != null) 
			item = Item.find.byId(id);
		else 
			item = SecureModelFactory.createNewItem();

		// immutable, returns a new form!
		Form<Item> form = form(Item.class).bindFromRequest().fill(item);
	
	    return ok(editItem.render(user(), form));
	}

	public static Result putUser() {

		User user = user();

		Form<User> formFromRequest = form(User.class).bindFromRequest();

		Logger.debug(formFromRequest + "");
		
		if(formFromRequest.hasErrors()) {
			
		    return badRequest(editProfil.render(user, formFromRequest));
		} 
	
		String orgPassword = getFieldFromRequestForm(User.class, "password");
		String new1Password = getFieldFromRequestForm(User.class, "new1_password");
		String new2Password = getFieldFromRequestForm(User.class, "new2_password");
		
		if(HashUtil.isSamePassword(user.password, user.salt, orgPassword))
		if(new1Password.equals(new2Password)) {
			
			User updated = formFromRequest.get();
			
			HashUtil.hashAndSalt(updated);		
			HashUtil.updatePassword(user, new1Password);
			
			updated.activities = user.activities;
			updated.items = user.items;
			updated.kinds = user.kinds;
			updated.password = user.password;
			updated.salt = user.salt;
			
			updated.update(user.id);
			
		    return returnToReferer();
		}
		
		return unauthorized("Wrong password");
	}

	public static Result putActivity() {
		
		Long id = getIdFromRequestForm(Activity.class);
		User user = user();
		
		Activity activity;

		boolean created = false;
		
		if(id != null) {

			activity = Activity.find.byId( id );
		}
		else {

			activity = SecureModelFactory.createNewActivity(user);
			user.activities.add(activity);
			created = true;
		}
		
		Form<Activity> form = form(Activity.class).fill(activity).bindFromRequest();
		
		if(form.hasErrors()) {
			
		    return badRequest(editActivity.render(user, form));
		}

		Activity updated = SecureModelFactory.updateActivity(activity, form, user, request());

		user.save();
		updated.update(activity.id);
		
		if(created)
			 viewCreated();
		return viewUpdated();
	}

	public static Result putKind() {
		
		Long id = getIdFromRequestForm(Kind.class);
		User user = user();

		Kind kind;
	
		boolean created = false;
		
		if(id != null) {

			kind = Kind.find.byId( id );
		}
		else {

			kind = SecureModelFactory.createNewKind();
			user.kinds.add(kind);
			created = true;
		}
		
		Form<Kind> form = form(Kind.class).fill(kind).bindFromRequest();
		
		if(form.hasErrors()) {
			
		    return badRequest(editKind.render(user, form));
		} 

		Kind updated = SecureModelFactory.updateKind(form);

		user.save();
		updated.update(kind.id);
		
		if(created)
			 viewCreated();
		return viewUpdated();
	}
    
	public static Result putItem() {
		
		Long id = getIdFromRequestForm(Item.class);
		User user = user();

		Item item;
		
		boolean created = false;

		if(id != null) {

			item = Item.find.byId( id );
		}
		else {

			item = SecureModelFactory.createNewItem();
			user.items.add(item);
			created = true;
		}
		
		Form<Item> form = form(Item.class).fill(item).bindFromRequest();
		
		if(form.hasErrors()) {
			
		    return badRequest(editItem.render(user, form));
		} 

		Item updated = SecureModelFactory.updateItem(form); 

		user.save();
		updated.update(item.id);
		
		if(created)
			 viewCreated();
		return viewUpdated();
	}

	public static Result deleteUser() {
	
		SecureModelFactory.deleteUser(user().id);
		
		return redirect(routes.Application.viewHome());
	}

	public static Result deleteActivity() {
	
		long id = getIdFromRequestQueryString();
		User user = user();
		SecureModelFactory.deleteActivity(id, user);
		return redirect(routes.Tad.viewDeleted());
	}

	public static Result deleteKind() {
	    
		long id = getIdFromRequestQueryString();
		User user = user();
		SecureModelFactory.deleteKind(id, user);
		return redirect(routes.Tad.viewDeleted());
	}

	public static Result deleteItem() {
        
    	long id = getIdFromRequestQueryString();
    	User user = user();
    	SecureModelFactory.deleteItem(id, user);
    	return redirect(routes.Tad.viewDeleted());
    }

	public static Result viewItems() {

    	return ok(viewItems.render(user()));
    }
    
    public static Result viewKinds() {
	
		return ok(viewKinds.render(user()));
	}

	public static Result viewActivities() {
	
		return ok(viewActivities.render(user()));
	}

	public static Result getActivities() {
	
		ExpressionList<Activity> activities = Activity.find.where().eq("t0.user_id", user().id);
		
		String kindId = getQueryString("kind");
		if(kindId != null)
			activities = activities.where().eq("kind_id", kindId);
		
		String itemId = getQueryString("item");
		if(itemId != null)
			activities = activities.where().eq("items.id", itemId);
		
		String fromDate = getQueryString("from");
		if(fromDate != null)
			activities = activities.where().gt("action_at", DateHelper.toDate(fromDate));
		
		String toDate = getQueryString("to");
		if(toDate != null)
			activities = activities.where().lt("action_at", DateHelper.toDate(toDate));
	
		String iDisplayLengthParam = getQueryString("iDisplayLength");
		String iDisplayStartParam = getQueryString("iDisplayStart");
		String sEchoParam = getQueryString("sEcho");
		
		ObjectNode json = null;
		
		if(iDisplayLengthParam != null && iDisplayStartParam != null && sEchoParam != null) {
			
	    	int iDisplayLength = Integer.parseInt( iDisplayLengthParam );
	    	int iDisplayStart = Integer.parseInt( iDisplayStartParam );
	    	int sEcho = Integer.parseInt( sEchoParam );
	    	int pageStart = iDisplayStart;
	    	int pageOffset = iDisplayStart + iDisplayLength;
		
			json = listToDataTable(activities.orderBy("action_at desc").orderBy("id desc").findList(), pageStart, pageOffset, sEcho);
		}
		else
			json = listToDataTable(activities.orderBy("action_at desc").findList());
	
		return ok(new JsonPrinter().print(json));
	}

	public static Result getItems() {

		ExpressionList<Item> items = Item.find.where().eq("t0.user_id", user().id);
		
		String iDisplayLengthParam = getQueryString("iDisplayLength");
		String iDisplayStartParam = getQueryString("iDisplayStart");
		String sEchoParam = getQueryString("sEcho");
		
		ObjectNode json = null;
		
		if(iDisplayLengthParam != null && iDisplayStartParam != null && sEchoParam != null) {
			
	    	int iDisplayLength = Integer.parseInt( iDisplayLengthParam );
	    	int iDisplayStart = Integer.parseInt( iDisplayStartParam );
	    	int sEcho = Integer.parseInt( sEchoParam );
	    	int pageStart = iDisplayStart;
	    	int pageOffset = iDisplayStart + iDisplayLength;
    	
	    	json = listToDataTable(items.orderBy("id desc").findList(), pageStart, pageOffset, sEcho);
		}
		else
			json = listToDataTable(items.orderBy("id desc").findList());

		return ok(new JsonPrinter().print(json));
    }

	public static Result getKinds() {
	
		ExpressionList<Kind> kinds = Kind.find.where().eq("t0.user_id", user().id);
		
		String iDisplayLengthParam = getQueryString("iDisplayLength");
		String iDisplayStartParam = getQueryString("iDisplayStart");
		String sEchoParam = getQueryString("sEcho");
		
		ObjectNode json = null;
		
		if(iDisplayLengthParam != null && iDisplayStartParam != null && sEchoParam != null) {
			
	    	int iDisplayLength = Integer.parseInt( iDisplayLengthParam );
	    	int iDisplayStart = Integer.parseInt( iDisplayStartParam );
	    	int sEcho = Integer.parseInt( sEchoParam );
	    	int pageStart = iDisplayStart;
	    	int pageOffset = iDisplayStart + iDisplayLength;
		
			json = listToDataTable(kinds.orderBy("id desc").findList(), pageStart, pageOffset, sEcho);
		}
		else
			json = listToDataTable(kinds.orderBy("id desc").findList());
	
		return ok(new JsonPrinter().print(json));
	}

	public static Result deleteItemImage() {
	
		Form<Item> formFromRequest = form(Item.class).bindFromRequest();
		Long id = Long.parseLong(formFromRequest.field("id").value());
		
		Logger.debug("delete image of item = " + id);
		
		Item item = user().getItem(id);
		item.picture = FileHelper.toByteArray("public/images/no_image.png");
		item.update();

		return returnToReferer();
	}
	
	public static Result deleteUserImage() {

		User user = user();
		
		Logger.debug("delete image of user = " + user.id);

		user.picture = FileHelper.toByteArray("public/images/no_image.png");
		user.update();

		return returnToReferer();
	}
	
	public static Result postItemImage() {
		
		MultipartFormData body = request().body().asMultipartFormData();
		
		FilePart picture = body.getFile("image");
	
		Form<Item> formFromRequest = form(Item.class).bindFromRequest();
		
		User user = user();
		
		Item item;
		
		if(formFromRequest.field("id").value() == null) {
			
			item = SecureModelFactory.createNewItem();
			
			user.items.add(item);
			user.save();
		}
		else {
		
			Long id = Long.parseLong(formFromRequest.field("id").value());
			
			Logger.debug("attach image to item = " + id);
			
			item = user.getItem(id);
		}
		
		item.picture = FileHelper.toByteArray(picture);		
		item.update();

		return returnToReferer();
	}
	
	public static Result postUserImage() {
		
		MultipartFormData body = request().body().asMultipartFormData();
		
		FilePart picture = body.getFile("image");
		
		User user = user();
	
		user.picture = FileHelper.toByteArray(picture);
		user.update();
	
		return returnToReferer();
	}

	public static Result serveUserImage() {

		if(user() != null)
		if(user().picture != null)
			return ok( user().picture );
		
		return ok();
    }
	
	public static Result serveItemImage() {

		Long id = getIdFromRequestQueryString();
		
		if(id != null) {

			if(id != null)
			if(user().getItem(id) != null)
			if(user().getItem(id).picture != null)
				return ok( user().getItem(id).picture );
		}
		
		return ok();
    }
    
    private static User user() {
		
		return User.find.where().eq("email", request().username()).findUnique();
	}

    
    private static String getReferer() {
    	
    	return request().getHeader("referer");
    }
    
	private static Result returnToReferer() {

    	try{ 
    		
    		return redirect(getReferer()); 
    	} 
    	catch (Exception e) { 
    		
    		return redirect(routes.Tad.viewDashboard());
    	} 
	}

    private static Long getIdFromRequestQueryString() {

    	String id = request().getQueryString("id");
    	
    	Logger.debug("request query string id = " + id);
    	
    	if(id == null || id.equals(""))
    		return null;
    	
		return Long.parseLong(id);
	}

    private static Long getIdFromRequestForm(Class<?> clazz) {

    	String id = getFieldFromRequestForm(clazz, "id");
    	
    	if(id == null || id.equals(""))
    		return null;
    	
    	return Long.parseLong(id);
    }
    
    private static String getFieldFromRequestForm(Class<?> clazz, String field) {
    	
    	Form<?> form = form(clazz).bindFromRequest();
    	String value = form.field(field).value();
    	
    	Logger.debug("request form " + field + " = " + value);
    	
    	return value;
    }
    
    private static int adjust(int value, int size) {

    	if(value >= size)
    		value = size;
    	return value;
	}
    
	private static ObjectNode listToDataTable(List<?> list) {

    	ObjectNode result = Json.newObject();
    	result.put("iTotalRecords", list.size());
    	result.put("iTotalDisplayRecords", list.size());
    	result.put("aaData", Json.toJson(list));
    	
		return result;
	}

    private static ObjectNode listToDataTable(List<?> list, int from, int to, int sEcho) {

    	from = adjust(from, list.size());
    	to = adjust(to, list.size());
    	
    	ObjectNode result = Json.newObject();
    	result.put("iTotalRecords", list.size());
    	result.put("iTotalDisplayRecords", list.size());
    	result.put("sEcho", sEcho);
    	result.put("aaData", Json.toJson(list.subList(from, to)));
    	
		return result;
	}
    
	private static String getQueryString(String string) {
		
		return request().getQueryString(string);
	}
}