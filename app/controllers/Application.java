package controllers;

import static play.data.Form.form;

import java.util.List;

import models.Activity;
import models.Item;
import models.Kind;
import models.User;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.FileHelper;
import utils.Mocker;
import views.html.about;
import views.html.api;
import views.html.index;
import views.html.login;
import views.html.secure;
import views.html.signup;
import authentication.HashUtil;
import authentication.Login;

public class Application extends Controller {
	
	private static User user() {
		
		return User.find.where().eq("email", request().username()).findUnique();
	}
	
    //
    //
    //	VIEWS
    //
    //

    public static Result viewApi() {

        return ok(api.render(null));
    }
    
    public static Result viewAbout() {

        return ok(about.render(null));
    }
    
    public static Result viewLogin() {

        return ok(login.render(form(Login.class)));
    }
    
    public static Result viewSignup() {
    	
        return ok(signup.render(form(User.class)));
    }
    
    public static Result viewHome() {

    	if(user() != null) {
    		
    		return redirect(routes.Tad.viewDashboard());
    	}
    	
    	List<User> users = User.find.all();
    	List<Activity> activities = Activity.find.all();
    	List<Kind> kinds = Kind.find.all();
    	List<Item> items = Item.find.all();
    	
        return ok(index.render(null, form(Login.class), form(User.class), users.size(), activities.size(), kinds.size(), items.size()));
    }
    
    public static Result viewSecure() {

        return ok(secure.render(null));
    }
    
    //
    //
    //	ACTIONS
    //
    //
    
    public static Result postUser() {

    	Form<User> form = form(User.class).bindFromRequest();
    	
    	if(form.hasErrors()) {
    		
    		Logger.debug("invalid user signup form = " + form);
    	    return badRequest(signup.render(form));
    	} 
    
    	User user = form.bindFromRequest().get();
    	user.kinds.add(Kind.defaultKind);
    	user.picture = FileHelper.toByteArray("public/images/no_image.png");
    	
    	HashUtil.hashAndSalt(user);
    	
	    user.save();

	    session("email", user.email);
	    
	    Logger.debug("new user created");
	    
	    return redirect(routes.Tad.viewDashboard());
    }
    
    public static Result login() {

        Form<Login> form = form(Login.class).bindFromRequest();
   
        Logger.debug("doing login for user = " + form.field("email").value());
        
        if(form.hasErrors()) {
        	
            return badRequest(login.render(form));
        } 

        session("email", form.get().email);
            
        return redirect(routes.Tad.viewDashboard());
    }

    public static Result logout() {

        session().clear();
        
        flash("success", "You've been logged out");
        
        return redirect(routes.Application.viewHome());
    }

    public static Result mock() {

    	// only available in development mode
    	if(play.api.Play.isDev(play.api.Play.current()))
    		return ok(new Mocker().mock());
    	return notFound();
    }
}
