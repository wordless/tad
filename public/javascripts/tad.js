var tad = {};

(function() {
	
	console.log("making tad js");
	
	tad.sayHello = function() {
		
		console.log("hello");
	}
	
	tad.makeGpsVisualization = function(kml) {

		/*
		 * function global settings and variables used
		 * for the charts
		 */
		var path;
		var SAMPLES = 200;
		var elevationService = null;
		var chart = null;
		var mousemarker = null;
		var infowindow = null;
		
		google.load("visualization", "1", {packages: ["columnchart"]});
		google.setOnLoadCallback(initialize);
		
		function initialize() {
	
			chart = new google.visualization.ColumnChart(document.getElementById('elevation_chart'));
			
			elevationService = new google.maps.ElevationService();
			
			infowindow = new google.maps.InfoWindow({});

			google.visualization.events.addListener(chart, 'onmouseover', function(e) {
			
				if (mousemarker == null) {
				
					mousemarker = new google.maps.Marker({
						
						position: elevations[e.row].location,
						map: map,
						icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
			        });
			        
			        var contentStr = "elevation="+elevations[e.row].elevation+"<br>location="+elevations[e.row].location.toUrlValue(6);
				
					mousemarker.contentStr = contentStr;
					
					google.maps.event.addListener(mousemarker, 'click', function(evt) {
				  		//mm_infowindow_open = true;
			         	infowindow.setContent(this.contentStr);
				  		infowindow.open(map,mousemarker);
			        });
				} 
			    else {
			    	
			    	var contentStr = "elevation="+elevations[e.row].elevation+"<br>location="+elevations[e.row].location.toUrlValue(6);
					mousemarker.contentStr = contentStr;
			        infowindow.setContent(contentStr);
			        mousemarker.setPosition(elevations[e.row].location);
			       	//if (mm_infowindow_open) infowindow.open(map,mousemarker);
			    }
			});

		    var mapOptions = {
		    		
				center: new google.maps.LatLng(-34.397, 150.644),
		        zoom: 8,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    
		    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		
		    var parser = new geoXML3.parser({
			    map: map, 
			    processStyles: true,
			    afterParse: useTheData
		    });
	
			parser.parseKmlString(kml);
	
			function useTheData(doc){
			
				geoXmlDoc = doc[0];
			  	for (var i = 0; i < geoXmlDoc.placemarks.length; i++) {
			    
			    	var placemark = geoXmlDoc.placemarks[i];
			    	
			    	if (placemark.polyline) {
			      		if (!path) {
			      			
			        		path = [];
			        		var samples = placemark.polyline.getPath().getLength();
			        		var incr = samples/SAMPLES;
			        		
			        		if (incr < 1) 
			        			incr = 1;
			        		
					        for (var i=0;i<samples; i+=incr) {
					        	
					        	path.push(placemark.polyline.getPath().getAt(parseInt(i)));
					        }
				      	}								 
			    	}
			  	}
			  	
			  	drawPath(path);
			}

			function drawPath(path) {
			
				var pathRequest = {
				
		        	path: path,
		          	samples: SAMPLES
		        }
				
				elevationService.getElevationAlongPath(pathRequest, plotElevation);
			}
			
			function plotElevation(results, status) {
			
				elevationReqActive = false;
				
				if (status == google.maps.ElevationStatus.OK) {
					elevations = results;
			
				    var data = new google.visualization.DataTable();
				    
				    data.addColumn('string', 'Sample');
				    data.addColumn('number', 'Elevation');
				    
				    for (var i = 0; i < results.length; i++) {
				          
				     	data.addRow(['', elevations[i].elevation]);
				    }
				
				    document.getElementById('elevation_chart').style.display = 'block';
				    
				    chart.draw(data, {
				            width: 800,
				            height: 200,
				            legend: 'none',
				            titleY: 'Elevation (m)'
				    });
				}
			}
	    }
	}
	
	tad.makeKindTable = function(id, source) {

		return function() {
			
			$(id).dataTable({
		
		        "bProcessing": true,
		        "bServerSide": true,
		        "bSort": false,
		        "bFilter": false,
		        "sAjaxSource": source,
		        "sPaginationType": "full_numbers",
		        "aoColumns": [
		            { "mData": "id" },
		            { "mData": function( data, type, row ) { 
		            	
				                	var date = new Date(data['createdAt']);
				                	
				                	var d = date.getDate();
				                    var m = date.getMonth() + 1;
				                    var y = date.getFullYear();
				
				                	return d +  "/" + m + "/" + y; 
		            } },
		            { "mData": "name" },
		            { "mData": function( data, type, row ) { 
		            	
		            	return data['used']; 
		            } },
		            { "mData": function( data, type, row ) { 
		            	 
		            	return "<img width='16px' src='/assets/images/icons/" + data['icon'] + "'>"; 
		            } },
		            { "mData": function( data, type, row ) { 
		            	 
		            	/*
		            	// can't delete the default one!
		            	if(data.name === "Default") {
		            		
		            		return "<div class='actions'>" +
	            		   	   	   "<a class='foundicon-settings' href='/tad/edit/kind?id=" + data['id'] + "'></a>" +
	            		   	   	   "<a class='foundicon-add-doc' href='/tad/clone/kind?id=" + data['id'] + "'></a>" +
	            		   	   	   "<a class='foundicon-graph' href='/tad/view/kind?id=" + data['id'] + "'></a>" +
	            		   	   	   "</div>";
		            	}
		            	*/
		            	
		            	return "" +
		            		   "<div id='delete' class='reveal-modal small'>" + 
		            		   "	<h2>Do you really want to delete this kind?</h2>" + 
							   "	<a href='/tad/delete/kind?id=" + data['id'] + "' class='button commit-button'>Delete!</a>" + 
							   "	<a class='close-reveal-modal'>&#215;</a>" + 
							   "</div>" +
		            		   "<div class='actions'>" +
	            		   	   "<a class='foundicon-settings' href='/tad/edit/kind?id=" + data['id'] + "'></a>" +
	            		   	   "<a class='foundicon-add-doc' href='/tad/clone/kind?id=" + data['id'] + "'></a>" +
		            		   "<a class='foundicon-remove' href='#' data-reveal-id='delete'></a>" +
	            		   	   "<a class='foundicon-graph' href='/tad/view/kind?id=" + data['id'] + "'></a>" +
	            		   	   "</div>";
		            } }
		        ]
			});
		}
	}
	
	tad.makeItemTable = function(id, source) {
		
		return function() {
	
			$(id).dataTable({
		    	
		        "bProcessing": true,
		        "bServerSide": true,
		        "bSort": false,
		        "bFilter": false,
		        "sAjaxSource": source,
		        "sPaginationType": "full_numbers",
		        "aoColumns": [
		            { "mData": "id" },
		            { "mData": function( data, type, row ) { 
		            	
				                	var date = new Date(data['createdAt']);
				                	
				                	var d = date.getDate();
				                    var m = date.getMonth() + 1;
				                    var y = date.getFullYear();
				
				                	return d +  "/" + m + "/" + y; 
		            } },
		            { "mData": "name" },
		            { "mData": function( data, type, row ) { 
		            	
		            	return data['used']; 
		            } },
		            { "mData": function( data, type, row ) { 
		            	
		            	return "<img width='64px' src='/tad/image/item/serve?id=" + data['id'] + "'>"; 
		            } },
		            { "mData": function( data, type, row ) { 
		            	 
		            	return "" +
		            		   "<div id='delete' class='reveal-modal small'>" + 
		            		   "	<h2>Do you really want to delete this item?</h2>" + 
							   "	<a href='/tad/delete/item?id=" + data['id'] + "' class='button commit-button'>Delete!</a>" + 
							   "	<a class='close-reveal-modal'>&#215;</a>" + 
							   "</div>" +
						   	   "<div class='actions'>" +
	            		   	   "<a class='foundicon-settings' href='/tad/edit/item?id=" + data['id'] + "'></a>" +
	            		   	   "<a class='foundicon-add-doc' href='/tad/clone/item?id=" + data['id'] + "'></a>" +
	            		   	   "<a class='foundicon-remove' href='#' data-reveal-id='delete'></a>" +
	            		   	   "<a class='foundicon-graph' href='/tad/view/item?id=" + data['id'] + "'></a>" +
	            		   	   "</div>";
		            } }
		        ]
			});
		}
	}
	
	tad.makeActivityTable = function(id, source) {
		
		return function() {
		
		    $(id).dataTable({
		    	
		        "bProcessing": true,
		        "bServerSide": true,
		        "bSort": false,
		        "bFilter": false,
		        "sAjaxSource": source,
		        "sPaginationType": "full_numbers",
		        "aoColumns": [
		            { "mData": "id" },
		            { "mData": function( data, type, row ) { 
		            	
		            	if( data['date']) {
		            	    
			            	var date = new Date(data['actionAt']);
			            	
			            	var d = date.getDate();
			                var m = date.getMonth() + 1;
			                var y = date.getFullYear();
			
			            	return d +  "/" + m + "/" + y; 
		            	}
		            	
		            	return "";
		            } },
		            { "mData": function( data, type, row ) { 
		            	
				                	var date = new Date(data['createdAt']);
				                	
				                	var d = date.getDate();
				                    var m = date.getMonth() + 1;
				                    var y = date.getFullYear();
				
				                	return d +  "/" + m + "/" + y; 
		            } },
		            { "mData": "comment" },
		            { "mData": function( data, type, row ) { 

		            	return data['durationInMin'];
		            } }, 
		            { "mData": function( data, type, row ) { 
		
			            return data['distanceInKm']; 
		            } },       
		            { "mData": function( data, type, row ) { 
		            	 
		            	if( data['hasKml'] ) {
		            		
		            		return "<span class='success label'>KML</span>"
		            	}
		            	
		            	return ""
		            } }
		            ,            
		            { "mData": function( data, type, row ) { 
		 
		            	return "<img width='16px' src='/assets/images/icons/" + data.kind.icon + "'>";
		            } },
		            { "mData": function( data, type, row ) { 
		       		 
		            	return "" +
		            		   "<div id='delete' class='reveal-modal small'>" + 
		            		   "	<h2>Do you really want to delete this activity?</h2>" + 
							   "	<a href='/tad/delete/activity?id=" + data['id'] + "' class='button commit-button'>Delete!</a>" + 
							   "	<a class='close-reveal-modal'>&#215;</a>" + 
							   "</div>" +
							   "<div class='actions'>" +
		            		   "<a class='foundicon-settings' href='/tad/edit/activity?id=" + data['id'] + "'></a>" +
		            		   "<a class='foundicon-add-doc' href='/tad/clone/activity?id=" + data['id'] + "'></a>" +
		            		   "<a class='foundicon-remove' href='#' data-reveal-id='delete'></a>" +
		            		   "<a class='foundicon-graph' href='/tad/view/activity?id=" + data['id'] + "'></a>" +
		            		   "</div>";
		            } }
		        ]
		    });
		}
	}
})();
